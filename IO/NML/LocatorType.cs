using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace WCT.Core.Graphing.GraphLib.IO.NML
{
    /// <summary>
    /// XML wrapper for the locator type
    /// </summary>
    [XmlType(IncludeInSchema = true, TypeName = "locator.type")]
    [XmlRoot(ElementName = "locator", IsNullable = false, DataType = "")]
    public class LocatorType { }
}