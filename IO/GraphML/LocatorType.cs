
    using System;
    using System.Xml;
    using System.Xml.Serialization;
    using System.IO;
    
    
    
namespace WCT.Core.Graphing.GraphLib.IO.GraphML {    
    [XmlType(IncludeInSchema=true, TypeName="locator.type")]
    [XmlRoot(ElementName="locator", IsNullable=false, DataType="")]
    public class LocatorType {
    }
}
