using System;
using System.Collections;

namespace WCT.Core.Graphing.GraphLib
{
    /// <summary>
    /// Utility class to keep track which shapes have been visited or positioned (during the layout process).
    /// </summary>
    internal class VisitCollection : DictionaryBase
    {
        public bool this[string uid]
        {
            get { return (bool)this.InnerHashtable[uid]; }
            set { this.InnerHashtable[uid] = value; }
        }
    }
}