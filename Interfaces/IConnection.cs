using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using WCT.Core.Graphing.GraphLib;
using WCT.Core.Graphing.GraphLib.Configuration;

namespace WCT.Core.Graphing.GraphLib.Interfaces
{
    /// <summary>
    /// Connection interface
    /// </summary>
    public interface IConnection : IEntity
    {
        #region Properties

        /// <summary>
        /// Gets or sets the line-path (rectangular, Bezier...) of the connection
        /// </summary>
        string LinePath { get; set; }

        /// <summary>
        /// Gets or sets the line-style (dashed, continuous...) of the connection
        /// </summary>
        DashStyle LineStyle { get; set; }

        /// <summary>
        /// Gets or sets the line-end (arrows etc.) of the connection
        /// </summary>
        ConnectionEnd LineEnd { get; set; }

        /// <summary>
        /// Gets or sets the line-color of the connection
        /// </summary>
        Color LineColor { get; set; }

        /// <summary>
        /// Gets or sets the start-connector of the connection
        /// </summary>
        Connector From { get; set; }

        /// <summary>
        /// Gets or sets the end-connector of the connection
        /// </summary>
        Connector To { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Adds a connection point to the collection of intermediate connection points
        /// </summary>
        /// <param name="point"></param>
        void AddConnectionPoint(PointF point);

        /// <summary>
        /// Removes an intermediate connection point
        /// </summary>
        /// <param name="point"></param>
        void RemoveConnectionPoint(PointF point);

        #endregion Methods
    }
}