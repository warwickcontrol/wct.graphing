using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace Netron.UI
{
	/// <summary>
	/// Control displaying the properties of the Netron elements
	/// </summary>
	public class NetronProperties : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.PropertyGrid _PropertyGrid;
		private Netron.UI.NetronPanel _NetronPanel = null;

		
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Default constructor
		/// </summary>
		public NetronProperties()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
		}

		/// <summary>
		/// Gets the property grid object displaying the properties of an object.
		/// </summary>
		public PropertyGrid Grid
		{
			get { return _PropertyGrid; }
		}

		/// <summary>
		/// Gets or sets the associated netron panel
		/// </summary>
		public NetronPanel Panel
		{
			get { return _NetronPanel; }
			set 
			{
				if( value != _NetronPanel )
				{
					// Detach event handler if necessary
					if( _NetronPanel != null )
						_NetronPanel.SelectEvent -= new Netron.StatusEventHandler(this.EntitySelectEvent);
					
					// Remember panel and attach event handler
					_NetronPanel = value; 
					_NetronPanel.SelectEvent += new Netron.StatusEventHandler(this.EntitySelectEvent);
				}
			}
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( netronPanel1 != null )
				{
					netronPanel1.SelectEvent -= new Netron.StatusEventHandler(this.netronPanel1_SelectEvent);
				}
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._PropertyGrid = new System.Windows.Forms.PropertyGrid();
			this.SuspendLayout();
			
			this._PropertyGrid.CommandsVisibleIfAvailable = true;
			this._PropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this._PropertyGrid.LargeButtons = false;
			this._PropertyGrid.LineColor = System.Drawing.SystemColors.ScrollBar;
			this._PropertyGrid.Name = "_PropertyGrid";
			this._PropertyGrid.Size = new System.Drawing.Size(264, 320);
			this._PropertyGrid.TabIndex = 0;
			this._PropertyGrid.Text = "_PropertyGrid";
			this._PropertyGrid.ViewBackColor = System.Drawing.SystemColors.Window;
			this._PropertyGrid.ViewForeColor = System.Drawing.SystemColors.WindowText;
			this._PropertyGrid.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.PropertyValueChanged);
			// 
			// NetronProperties
			// 
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
			this._PropertyGrid});
			this.Name = "NetronProperties";
			this.Size = new System.Drawing.Size(264, 320);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// A property value has changed. Signal selected entity in the panel to update.
		/// </summary>
		/// <param name="s"></param>
		/// <param name="e"></param>
		private void PropertyValueChanged(object s, System.Windows.Forms.PropertyValueChangedEventArgs e)
		{
			Netron.Entity entity = (Netron.Entity) this._PropertyGrid.SelectedObject;
			entity.Invalidate();
		}

		/// <summary>
		/// A new entity has been selected in the panel, display the properties accordingly
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void EntitySelectEvent(object sender, Netron.StatusEventArgs e)
		{
			this._PropertyGrid.SelectedObject = (e.Status==EnumStatusType.Selected)? e.Entity : null;
		}

	}
}
