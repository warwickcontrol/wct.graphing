using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
//using System.Runtime.Serialization.Formatters.Soap;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using Netron.GraphLib.UI;

namespace Netron.GraphLib
{
	

	
	

	
	

	public class Vector2D
	{
		public double x, y, z;

		/*-----------------------------------*/
		/* Constructors                      */
		/*-----------------------------------*/

		public Vector2D() 
		{
			x=0; y=0; z=0;
		} // end Vector2D.

		public Vector2D(Vector2D v) 
		{
			x=v.x; y=v.y; z=v.z;
		} // end Vector2D.

		public Vector2D(double ax, double ay, double az) 
		{
			x=ax; y=ay; z=az;
		} // end Vector2D.

		/*-----------------------------------*/
		/* Simple Operations                 */
		/*-----------------------------------*/
		public Vector2D add(Vector2D v) 
		{     
			return new Vector2D(x+v.x,y+v.y,z+v.z);
		}

		public void addEquals(Vector2D v) 
		{
			x += v.x; y += v.y; z += v.z;
		}

		public Vector2D sub(Vector2D v) 
		{
			return new Vector2D(x-v.x,y-v.y,z-v.z);
		}

		public void subEquals(Vector2D v) 
		{
			x -= v.x; y -= v.y; z -= v.z;
		}

		public Vector2D mul(double s) 
		{
			return new Vector2D(x*s,y*s,z*s);
		}

		public void mulEquals(double s) 
		{
			x = x*s; y = y*s; z = z*s;
		}

		public Vector2D div(double s) 
		{
			if (s!=0) 
			{
				return new Vector2D(x/s,y/s,z/s);
			} 
			else 
			{
				return new Vector2D();
			}
		}

		public void divEquals(double s) 
		{
			if (s!=0) 
			{
				x = x/s; y = y/s; z = z/s;
			}
		}

		public Vector2D negate() 
		{
			return new Vector2D(-x,-y,-z);
		}
   
		public void negateEquals() 
		{
			x = -x; y = -y; z = -z;
		}

		public void set(Vector2D v) 
		{
			x = v.x; y = v.y; z = v.z;
		}

		/*-----------------------------------*/
		/* Complex Operations                */
		/*-----------------------------------*/
		public double dotProduct(Vector2D v) 
		{
			return x*v.x+y*v.y+z*v.z;
		}

		public Vector2D proj(Vector2D v) 
		{
			return v.mul((this.dotProduct(v))/(v.dotProduct(v)));
		}

		public void projEquals(Vector2D v) 
		{
			this.set(v.mul((this.dotProduct(v))/(v.dotProduct(v))));
		}

		public Vector2D crossProduct(Vector2D v) 
		{
			return new Vector2D(this.y*v.z-v.y*this.z, this.z*v.x-v.z*this.x, this.x*v.y-v.x*this.y);
		}

		public void crossProductEquals(Vector2D v) 
		{
			this.set(new Vector2D(this.y*v.z-v.y*this.z, this.z*v.x-v.z*this.x, this.x*v.y-v.x*this.y));
		}

		/*-----------------------------------*/
		/* Boolean Operations                */
		/*-----------------------------------*/

		public Boolean equals(Vector2D v) 
		{
			if(this.x!=v.x) { return false; }
			if(this.y!=v.y) { return false; }
			if(this.z!=v.z) { return false; }
			return true;
		}

		/*-----------------------------------*/
		/* Handy Operations                  */
		/*-----------------------------------*/

		public double length() 
		{
			return Math.Sqrt(x*x+y*y+z*z);
		}

		public double lengthSquared() 
		{                         // good for comparing disances. faster than length.
			return x*x+y*y+z*z;
		}

		public Vector2D unit() 
		{
			return this.div(this.length());
		}

		public void setUnit() 
		{
			this.divEquals(this.length());
		}

		public void setLength(double s) 
		{
			this.unit().mul(s);
		}

		public String toString() 
		{
			
			return "(" + x + "," + y + "," + z + ")";
		}

		public double distance(Vector2D n)
		{
			return Math.Sqrt((x-n.x)*(x-n.x) + (y-n.y)*(y-n.y)+(z-n.z)*(z-n.z));
		}
		/// <summary>
		/// Returns the angle between two vectors
		/// </summary>
		/// <param name="v"></param>
		/// <returns></returns>
		public static double angle(Vector2D v1,Vector2D v2)
		{
			if((v1.length() >0) && (v2.length()>0))
				return Math.Acos(v1.dotProduct(v2)/(v1.length()*v2.length()));
			else
				return 0;
		}

		// rotate - Rotates a 3D point along the origin of its coordinate system

		public Vector2D rotate(double xr,double yr,double zr) 
		{//Euler angles
			Vector2D t = new Vector2D(); // Temporary variables for holding old values
			double sX = Math.Sin(xr), cX = Math.Cos(xr), //to avoid reduntant calculations.
				sY = Math.Sin(yr), cY = Math.Cos(yr),
				sZ = Math.Sin(zr), cZ = Math.Cos(zr);
			Vector2D tmp = new Vector2D(this);
    
			t.y = tmp.y*cX - tmp.z*sX;  //rotate around the x-axis
			t.z = tmp.y*sX + tmp.z*cX;
			tmp.y=t.y;              //update vars needed in algorithm
			tmp.z=t.z;

			t.x = tmp.x*cY - tmp.z*sY;  //rotate around the y-axis
			t.z =tmp. x*sY + tmp.z*cY;
			tmp.x=t.x;              //update vars needed in algorithm again
			tmp.z=t.z;

			t.x = tmp.x*cZ - tmp.y*sZ;  //rotate around the z-axis
			t.y = tmp.x*sZ + tmp.y*cZ;
			tmp.x=t.x;              //update vars for final storage
			tmp.y=t.y;
			return t;
		}

		public void rotateEquals(double xr,double yr,double zr) 
		{
			Vector2D t = new Vector2D(); // Temporary variables for holding old values
			double sX = Math.Sin(xr), cX = Math.Cos(xr), //to avoid reduntant calculations.
				sY = Math.Sin(yr), cY = Math.Cos(yr),
				sZ = Math.Sin(zr), cZ = Math.Cos(zr);

			t.y = y*cX - z*sX;  //rotate around the x-axis
			t.z = y*sX + z*cX;
			y=t.y;              //update vars needed in algorithm
			z=t.z;

			t.x = x*cY - z*sY;  //rotate around the y-axis
			t.z = x*sY + z*cY;
			x=t.x;              //update vars needed in algorithm again
			z=t.z;

			t.x = x*cZ - y*sZ;  //rotate around the z-axis
			t.y = x*sZ + y*cZ;
			x=t.x;              //update vars for final storage
			y=t.y;
			z=t.z;   
		}
	} // end Vector2D.


	

}