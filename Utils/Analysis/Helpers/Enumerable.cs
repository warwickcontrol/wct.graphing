using System;
using System.Collections;

namespace WCT.Core.Graphing.GraphLib.Analysis
{
    /// <summary>
    /// Decorates an IEnumerator implementation to an IEnumerable implementation
    /// </summary>
    public class Enumerable : IEnumerable
    {
        #region Field

        /// <summary>
        /// the enumerator of the Enumerable
        /// </summary>
        private IEnumerator enumerator;

        #endregion Field

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="enumerator"></param>
        public Enumerable(IEnumerator enumerator)
        {
            this.enumerator = enumerator;
        }

        #endregion Constructor

        #region Method

        /// <summary>
        /// Returns the IEnumerator enumerator
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerator GetEnumerator()
        {
            return enumerator;
        }

        #endregion Method
    }
}